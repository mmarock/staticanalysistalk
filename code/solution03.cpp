#include <iostream>
#include <vector>

struct Foo
{
    int bar_;
};

static void print(const Foo &f)
{
    std::cout << "(" << f.bar_ << ") Foo here" << std::endl;
}

int main()
{

    Foo *f = nullptr;
    int i = 5;

    if (f == nullptr)
    {
        f = new Foo{1};
        print(*f);

        f->bar_ = i;
        print(*f);
    }

    std::vector<int> cont = {1, 2, 3};
    for (int j : cont)
    {
        std::cout << j << std::endl;
    }

    return f == nullptr;
}
