

#include <stdio.h>

const char *test = NULL;

void from_stack() {
    char local[] = "stack";
    test = local;
}

void from_heap() {
    test = "heap";
}

int main() {

    from_heap();
    printf("1: %s\n", test);

    from_stack();
    printf("2: %s\n", test);

    return 0;
}
