#include <algorithm>
#include <iostream>
#include <memory>
#include <vector>

struct Inner {
    int id_;

    explicit Inner(int i) : id_(i) {
    }
};

class Outer {
    Inner *inner_;
    int constructed_;

    static int &constructed_cnt() {
        static int c(0);
        return c;
    }

public:
    Outer(int i) : inner_(new Inner(i)), constructed_(constructed_cnt()++) {
        std::cout << "(" << constructed_ << ")"
                  << "Constructed id: " << inner_->id_ << std::endl;
    }

    ~Outer() {
        std::cout << "(" << constructed_ << ")"
                  << " Destructed " << inner_->id_ << std::endl;
        delete inner_;
    }

    Outer(const Outer &rhs)
        : inner_(new Inner(*rhs.inner_)), constructed_(constructed_cnt()++) {
        std::cout << "(" << constructed_ << ")"
                  << "(Copy) constructed id: " << inner_->id_ << std::endl;
    }

    Outer &operator=(const Outer &rhs) {
        delete inner_;
        inner_ = new Inner(*rhs.inner_);

        constructed_ = constructed_cnt()++;

        std::cout << "(" << constructed_ << ")"
                  << " (Copy) assigned id: " << inner_->id_ << std::endl;
        return *this;
    }

    int id() const noexcept {
        return inner_->id_;
    }
};

template <class T>
const T &get(const typename std::vector<T> &elements, int id) {
    auto it = std::find_if(elements.begin(), elements.end(), [&](const T &obj){
                std::cout << "\t-> compare " << obj.id() << " and " << id << std::endl;
                return obj.id() == id;
            });
    if (it == elements.end()) {
        throw std::runtime_error("element not found");
    }
    return *it;
}

int main() {

    // container <- init
    std::vector<Outer> container = {99, 98, 97};

    // copy <- find var
    auto &c = get(container, 99);
    container[0] = c;
    return c.id();
}
