#include <iostream>
#include <vector>

struct Foo
{
    int bar_;
};

static void print(const Foo &f)
{
    std::cout << "(" << f.bar_ << ") Foo here" << std::endl;
}

int main(void)
{

    Foo *f = 0;
    int i = 5;

    if (f == 0)
    {
        f = new Foo{1};
        print(*f);

        f->bar_ = i;
        print(*f);
    }

    std::vector<int> cont = {1, 2, 3};
    for (size_t j = 0; j < cont.size(); ++j)
    {
        std::cout << cont[j] << std::endl;
    }

    return f == NULL;
}
