

#include <iostream>
#include <vector>

namespace my {

template<class ContainerType>
typename ContainerType::value_type sum(const ContainerType &cont) {
    using T = typename ContainerType::value_type;
    T s{};
    for (const T &val : cont) {
        s += val;
    }
    return s;
}

} // namespace my

int main() {

    std::vector<int> foo = {1,3,5};

    auto s1 = my::sum(foo);
    std::cout << s1 << std::endl;

    auto bar = std::move(foo);
    return my::sum(foo);
}
