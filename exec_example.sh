
#!/bin/sh

TIDY=$(which clang-tidy)

CHECKER=""
OPT=""
SOURCE=""
case $1 in 
    help|"")
        echo "$0 [help, list, 1..4] [print_line]"
        exit 1 ;;
    list)
        CHECKER='*'
        OPT="-list-checks" ;;
    1)
        CHECKER="clang-analyzer-cplusplus.SelfAssignment"
        #SOURCE="code/selfAssignment.cpp" ;;
        SOURCE="code/example01.cpp" ;;
    2)
        CHECKER="clang-*"
        #SOURCE="code/stackEscape.cpp" ;;
        SOURCE="code/example02.cpp" ;;
    3)
        CHECKER="modernize-*"
        OPT="-fix"
        #SOURCE="code/modernizeNull.cpp" ;;
        SOURCE="code/example03.cpp" ;;
    4)
        CHECKER="bugprone-use-after-move"
        #SOURCE="code/useAfterMove.cpp" ;;
        SOURCE="code/example04.cpp" ;;
esac


LINE="$TIDY -checks='$CHECKER' $OPT $SOURCE"

echo "+---------------------------------------"
echo "| Execute presentation $1"
echo "| >> $LINE"
echo "+---------------------------------------"

eval $LINE
